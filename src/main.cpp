#include <windows.h>
#include <psapi.h>

#include "TSHooks.hpp"

const char *aG         = "%.16g",
           *aGGG       = "%.16g %.16g %.16g",
           *aSGS       = "%s = %.16g;%s",
           *aBetweenG  = "Must be between %.16g and %.16g",
           *aGGGGGGG   = "%.16g %.16g %.16g %.16g %.16g %.16g %.16g",
           *aG_G       = "%.16g.%.16g",
           *aDGG       = "%d %.16g %.16g",
           *aDGGG      = "%d %.16g %.16g %.16g",
           *aDGGGG     = "%d %.16g %.16g %.16g %.16g",
           *aGGGG      = "%.16g %.16g %.16g %.16g",
           *aGGGGGG    = "%.16g %.16g %.16g %.16g %.16g %.16g",
           *aGGGGGGGGG = "%.16g %.16g %.16g %.16g %.16g %.16g %.16g %.16g %.16g",
           *a000GGGG   = "0 0 0 %.16g %.16g %.16g %.16g",
           *aDGGGGGG   = "%d %.16g %.16g %.16g %.16g %.16g %.16g";

ADDR laG,
     laGGG,
     laSGS,
     laBetweenG,
     laGGGGGGG,
     laG_G,
     laDGG,
     laDGGG,
     laDGGGG,
     laGGGG,
     laGGGGGG,
     laGGGGGGGGG,
     la000GGGG,
     laDGGGGGG;

ADDR getEyeTransformSizeLoc1, getEyeTransformSizeLoc2;

/*
To make this work I go through and patch all references to
the various %g format strings to use the ones above. However,
some of the format strings are used for dSscanf which cannot use
the %.16g format, so I have to skip patching those.
*/
ADDR dSscanfLoc;
static ADDR ImageBase;
static ADDR ImageSize;

#define PatchAll(len, patt, mask, repl, ignore) \
	PatchAllMatches(len, (char *)patt, (char *)mask, (char *)repl, ignore);

static void InitScanner()
{
	HMODULE module = GetModuleHandle(NULL);
	if (module) {
		MODULEINFO info;
		GetModuleInformation(GetCurrentProcess(), module, &info, sizeof(MODULEINFO));
		ImageBase = (ADDR)info.lpBaseOfDll;
		ImageSize = info.SizeOfImage;
	}
}

static bool CompareData(BYTE *data, BYTE *pattern, char *mask)
{
	for (; *mask; ++data, ++pattern, ++mask) {
		if (*mask == 'x' && *data != *pattern)
			return false;
	}

	return (*mask) == 0;
}

static int PatchAllMatches(unsigned int len, char *pattern, char *mask, char *replace, ADDR ignore)
{
	BYTE *j = NULL;
	ADDR callOffset = 0;
	ADDR functionAddr = 0;
	int numpatched = 0;

	for (ADDR i = ImageBase; i < ImageBase + ImageSize - len; ++i) {
		if (CompareData((BYTE *)i, (BYTE *)pattern, mask)) { //found match
			for (j = (BYTE *)i + 5; *j != 0xE8; ++j); //a call op will occur soon after (how soon is unknown due to different op sizes)
			callOffset = *(ADDR *)(j + 1); //relative address to function

			functionAddr = (ADDR)j + callOffset + 5; //absolute address of function
			if (functionAddr == ignore) //it's the address of dSscanf, so skip it
				continue;

			++numpatched;
			for (ADDR c = 0; c < len; ++c)
				BlPatchByte(i + c, replace[c]);
		}
	}

	return numpatched;
}

void patchStringRefs(ADDR stringLoc, const char *replace)
{
	++stringLoc; //add 1 due to using 00 as start of string flag

	char patt[5];
	patt[0] = 0x68;
	*(ADDR *)(&patt[1]) = stringLoc;

	char bytes[5];
	bytes[0] = 0x68;
	*(ADDR *)(&bytes[1]) = (ADDR)(&replace[0]);

	PatchAll(5, patt, "xxxxx", bytes, dSscanfLoc);
}

void returnStringRefs(ADDR replace, const char *string)
{
	patchStringRefs((ADDR)(&string[0]) - 1, (const char *)(replace + 1));
}

bool init()
{
	BlInit;

	InitScanner();

	BlScanHex(dSscanfLoc, "51 56 8B 74 24 10 33 D2");

	BlScanHex(getEyeTransformSizeLoc1, "8D 51 64 3B 15 ? ? ? ? 72 24 8B 0D ? ? ? ? 81 C2 ? ? ? ? 89 15 ? ? ? ? E8 ? ? ? ? 8B 0D ? ? ? ? A3 ? ? ? ? EB 05 A1 ? ? ? ? D9 44 24 24");
	BlScanHex(getEyeTransformSizeLoc2, "6A 64 56 E8 ? ? ? ? 8B 8C 24 ? ? ? ? 83 C4 44");

	BlPatchByte(getEyeTransformSizeLoc1 + 2, 0x7F);
	BlPatchByte(getEyeTransformSizeLoc2 + 1, 0x7F);

	//Using 00 as a flag for both start and end of string to ensure the proper address is found
	BlScanHex(laG,         "00 25 67 00");
	BlScanHex(laGGG,       "00 25 67 20 25 67 20 25 67 00");
	BlScanHex(laSGS,       "00 25 73 20 3D 20 25 67 3B 25 73 00");
	BlScanHex(laBetweenG,  "00 4D 75 73 74 20 62 65 20 62 65 74 77 65 65 6E 20 25 67 20 61 6E 64 20 25 67 00")
	BlScanHex(laGGGGGGG,   "00 25 67 20 25 67 20 25 67 20 25 67 20 25 67 20 25 67 20 25 67 00");
	BlScanHex(laG_G,       "00 25 67 2E 25 67 00");
	BlScanHex(laDGG,       "00 25 64 20 25 67 20 25 67 00");
	BlScanHex(laDGGG,      "00 25 64 20 25 67 20 25 67 20 25 67 00");
	BlScanHex(laDGGGG,     "00 25 64 20 25 67 20 25 67 20 25 67 20 25 67 00");
	BlScanHex(laGGGG,      "00 25 67 20 25 67 20 25 67 20 25 67 00");
	BlScanHex(laGGGGGG,    "00 25 67 20 25 67 20 25 67 20 25 67 20 25 67 20 25 67 00");
	BlScanHex(laGGGGGGGGG, "00 25 67 20 25 67 20 25 67 20 25 67 20 25 67 20 25 67 20 25 67 20 25 67 20 25 67 00");
	BlScanHex(la000GGGG,   "00 30 20 30 20 30 20 25 67 20 25 67 20 25 67 20 25 67 00");
	BlScanHex(laDGGGGGG,   "00 25 64 20 25 67 20 25 67 20 25 67 20 25 67 00");

	patchStringRefs(laG,         aG);
	patchStringRefs(laGGG,       aGGG);
	patchStringRefs(laSGS,       aSGS);
	patchStringRefs(laBetweenG,  aBetweenG);
	patchStringRefs(laGGGGGGG,   aGGGGGGG);
	patchStringRefs(laG_G,       aG_G);
	patchStringRefs(laDGG,       aDGG);
	patchStringRefs(laDGGG,      aDGGG);
	patchStringRefs(laDGGGG,     aDGGGG);
	patchStringRefs(laGGGG,      aGGGG);
	patchStringRefs(laGGGGGG,    aGGGGGG);
	patchStringRefs(laGGGGGGGGG, aGGGGGGGGG);
	patchStringRefs(la000GGGG,   a000GGGG);
	patchStringRefs(laDGGGGGG,   aDGGGGGG);

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");
	return true;
}

bool deinit()
{
	BlPatchByte(getEyeTransformSizeLoc1 + 2, 0x64);
	BlPatchByte(getEyeTransformSizeLoc2 + 1, 0x64);

	returnStringRefs(laG,         aG);
	returnStringRefs(laGGG,       aGGG);
	returnStringRefs(laSGS,       aSGS);
	returnStringRefs(laBetweenG,  aBetweenG);
	returnStringRefs(laGGGGGGG,   aGGGGGGG);
	returnStringRefs(laG_G,       aG_G);
	returnStringRefs(laDGG,       aDGG);
	returnStringRefs(laDGGG,      aDGGG);
	returnStringRefs(laDGGGG,     aDGGGG);
	returnStringRefs(laGGGG,      aGGGG);
	returnStringRefs(laGGGGGG,    aGGGGGG);
	returnStringRefs(laGGGGGGGGG, aGGGGGGGGG);
	returnStringRefs(la000GGGG,   a000GGGG);
	returnStringRefs(laDGGGGGG,   aDGGGGGG);

	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
