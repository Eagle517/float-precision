# Float Precision
A DLL that increases the floating point precision of TorqueScript for Blockland.

## Usage
Use the latest version of [RedBlocklandLoader](https://gitlab.com/Eagle517/redblocklandloader/-/releases) to easily load the DLL into the game.

## Issues
`ShapeBase::getEyeTransform()` uses a character buffer of length 100.  When using high precision floats, it cannot fit the entire result into that buffer.  As a result, it gets truncated.  This DLL increases the length of the buffer to 127.  While this still isn't long enough to fit the entire result, it fits all 7 floats with the last float having more precision than it would normally, albeit truncated.  Increasing the length of the buffer past 127 would be grossly complex.

## Building
This DLL relies on [TSFuncs](https://gitlab.com/Eagle517/tsfuncs) which you will need to build first.  Building with CMake is straightforward, although it has only been setup for MinGW.  A toolchain has been included for cross-compiling on Linux.
